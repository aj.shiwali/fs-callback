const fs = require("fs");
const path = require("path");

module.exports.fsmodule = (numberOfFiles, cb) => {
  if (arguments.length < 2) {
    let err = new Error("Invalid Arguments");
    cb(err);
  } else {
    fs.mkdir(path.join(__dirname, "JSONfiles"), (err) => {
      if (err) {
        cb(new Error("Directory creation failed"));
      } else {
        cb(null, "Directory creation successfull");

        for (let i = 1; i <= numberOfFiles; i++) {
          //console.log(i);
          fs.writeFile(
            path.join(__dirname, `./JSONfiles/file${i}.json`),
            "Hello world! This is file number ${i}",
            (err) => {
              if (err) {
                cb(err);
              } else {
                cb(null, `File ${i} has been created`);
                setTimeout(() => {
                  fs.unlink(
                    path.join(__dirname, `./JSONfiles/file${i}.json`),
                    (err) => {
                      if (err) {
                        cb(err);
                      } else {
                        cb(null, `File ${i} has been deleted`);
                      }
                    }
                  );
                }, 1000);
              }
            }
          );
        }
      }
      setTimeout(() => {
        fs.rmdir(path.join(__dirname, "JSONfiles"), (err) => {
          if (err) {
            cb(err);
          } else {
            cb(null, "directory deleted successfully");
          }
        });
      }, 2000);
    });
  }
};
