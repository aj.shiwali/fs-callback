const { fsmodule } = require("../practice");

fsmodule(5, (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
