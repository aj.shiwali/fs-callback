const { fsUsingAsync } = require("../problem2");

const lipsum = "data/lipsum.txt";

fsUsingAsync(lipsum, (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
