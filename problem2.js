const fs = require("fs");
const path = require("path");

module.exports.fsUsingAsync = (lipsum, cb) => {
  if (arguments.length < 2) {
    let err = new Error("Invalid Input");
    cb(err);
  } else {
    let fileName = [1, 2, 3];
    readingFile(lipsum, (err, data) => {
      if (err) {
        cb(err);
      } else {
        let uppercaseData = data.toUpperCase();
        //let newFile = fileName[0];
        writingToFile(fileName[0], uppercaseData, (err, data) => {
          if (err) {
            cb(err);
          } else {
            storingFileNames(fileName[0], (err, data) => {
              if (err) {
                cb(err);
              } else {
                let upperCaseLipsum = `./data/${fileName[0]}.txt`;
                readingFile(upperCaseLipsum, (err, data) => {
                  if (err) {
                    cb(err);
                  } else {
                    let lowerCaseDataSplitted = data
                      .toLowerCase()
                      .split(". ")
                      .join("\n");
                    writingToFile(
                      fileName[1],
                      lowerCaseDataSplitted,
                      (err, data) => {
                        if (err) {
                          cb(err);
                        } else {
                          storingFileNames(fileName[1], (err, data) => {
                            if (err) {
                              cb(err);
                            } else {
                              let lowerCaseLipsum = `./data/${fileName[1]}.txt`;
                              readingFile(lowerCaseLipsum, (err, data) => {
                                if (err) {
                                  cb(err);
                                } else {
                                  let lowerCaseLipsumSorted = data
                                    .split("\n")
                                    .sort()
                                    .join("\n");
                                  writingToFile(
                                    fileName[2],
                                    lowerCaseLipsumSorted,
                                    (err, data) => {
                                      if (err) {
                                        cb(err);
                                      } else {
                                        storingFileNames(
                                          fileName[2],
                                          (err, data) => {
                                            if (err) {
                                              cb(err);
                                            } else {
                                              let storedFileNames = `./data/filenames.txt`;
                                              readingFile(
                                                storedFileNames,
                                                (err, data) => {
                                                  if (err) {
                                                    cb(err);
                                                  } else {
                                                    storedData =
                                                      data.split("\n");
                                                    storedData.forEach(
                                                      (file) => {
                                                        if (file != "") {
                                                          setTimeout(() => {
                                                            deletingContent(
                                                              file,
                                                              (err, data) => {
                                                                if (err) {
                                                                  cb(err);
                                                                } else {
                                                                  cb(
                                                                    null,
                                                                    `deleted ${file}`
                                                                  );
                                                                }
                                                              }
                                                            );
                                                          }, 10000);
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              });
                            }
                          });
                        }
                      }
                    );
                  }
                });
              }
            });
          }
        });
      }
    });
  }
};
// reading the file using fs.readfile function

function readingFile(file, cb) {
  fs.readFile(file, "utf8", (err, data) => {
    if (err) {
      cb(err);
    } else {
      cb(null, data);
    }
  });
}

// writing to file using fs.writeFile function

function writingToFile(filename, data, cb) {
  fs.writeFile(path.join(__dirname, `./data/${filename}.txt`), data, (err) => {
    if (err) {
      cb(err);
    } else {
      cb(null, "file written successfully");
    }
  });
}

// storing name of the file to filenames.txt

function storingFileNames(filename, cb) {
  fs.appendFile(
    path.join(__dirname, `./data/filenames.txt`),
    `\n${filename}.txt`,
    (err) => {
      if (err) {
        cb(err);
      } else {
        cb(null, "filename stored");
      }
    }
  );
}

// deleting file contents using fs.unlink

function deletingContent(filename, cb) {
  fs.unlink(path.join(__dirname, `./data/${filename}`), (err) => {
    if (err) {
      cb(err);
    } else {
      cb(null, `${filename} has been deleted`);
    }
  });
}
